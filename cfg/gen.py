#!/usr/bin/env python

import networkx as nx
import sys
import random

n=0
nodes=[]

def st_line():
	global n
	global nodes
	G=nx.DiGraph()
	n += 1
	G.add_node(n)
	start=n
	for i in range(random.randint(1,10)):
		n += 1
		G.add_node(n)
		nodes.append(n)
		G.add_edge(n-1,n,weight=1)
	n += 1
	G.add_node(n)
	end=n
	G.add_edge(n-1,n,weight=1)
	return (G,start,end)
def branch():
	global n
	global nodes
	G=nx.DiGraph()
	n+=1
	start=n
	n+=1
	end=n
	G.add_node(start)
	G.add_node(end)
	#left branch
	n+=1
	G.add_edge(start,n,weight=1)
	for i in range(random.randint(0,10)):
		n += 1
		G.add_node(n)
		nodes.append(n)
		G.add_edge(n-1,n,weight=1)
	G.add_edge(n,end,weight=1)
	#right branch
	n+=1
	G.add_edge(start,n,weight=1)
	for i in range(random.randint(0,10)):
		n += 1
		G.add_node(n)
		nodes.append(n)
		G.add_edge(n-1,n,weight=1)
	G.add_edge(n,end,weight=1)
	return (G,start,end)
def loop():
	global n
	global nodes
	G=nx.DiGraph()
	n += 1
	G.add_node(n,weight=1)
	start = n
	for i in range(random.randint(1,10)):
		n += 1
		G.add_node(n)
		nodes.append(n)
		G.add_edge(n-1,n,weight=1)
	n += 1
	G.add_node(n)
	end=n
	G.add_edge(n-1,n,weight=1)
	G.add_edge(end,start,weight=0)
	return (G,start,end)
def main():	
	(G,start,end) = st_line()
	for i in range(random.randint(5,random.randint(10,15))):
		l=len(nodes)-1
		rnd = nodes.pop(random.randint(0,l))
		prev = G.predecessors(rnd)[0]
		next = G.successors(rnd)[0]
		s = random.randint(0,2)
		if s==0:
			(H,start,end)=st_line()
		elif s==1:
			(H,start,end)=branch()
		else:
			(H,start,end)=loop()
		G.remove_node(rnd)
		L=[]
		for (u,v,d) in H.edges(data=True):
			L.append((u,v,d['weight']))
		G.add_weighted_edges_from(L)
		G.add_edge(prev,start,weight=1)
		G.add_edge(end,next,weight=1)
	nx.drawing.nx_agraph.write_dot(G,"graph.dot")
	#print nodes
	
main()
