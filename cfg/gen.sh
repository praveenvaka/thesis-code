#!/bin/bash
for i in {1..25}
do
   ./gen.py
   mv graph.dot inputs/in_$i.dot
done

for i in ./inputs/*.dot
do
	dot -Tpng -o ./inputs/graphs/$(basename $i ".dot").png $i
done
