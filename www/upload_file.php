<?php

$upload_dir = "/var/www/upload/";
$upload_dir_rel = "upload/";
$work_dir = "/var/www/work/";
	
function genRandomString() {
    $length = 10;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';    
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters))];
    }
    return $string;
}

if(preg_match("/\.dot$/",$_FILES["file"]["name"])){
	if ($_FILES["file"]["error"] > 0){
		echo "Error: " . $_FILES["file"]["error"] . "<br />";
		exit();
	}else{
		do{
			$rand_string = genRandomString();
		}while(file_exists($upload_dir.$rand_string));
		$upload_dir .= $rand_string."/";
		$upload_dir_rel .= $rand_string."/";
		mkdir($upload_dir,0777,true);
		chmod($upload_dir,0777);
		if (file_exists($upload_dir.$_FILES["file"]["name"])){
			echo $_FILES["file"]["name"] . " already exists. ";
		}else{
			move_uploaded_file($_FILES["file"]["tmp_name"],$upload_dir."in.dot");
			exec('dot -Tpng -o '.$upload_dir.'in.png '.$upload_dir.'in.dot');
		}
	}
}else{
	echo 'Input File should have extension "dot". <a href="index.php">Click Here</a> to go back and try again.';
	exit();
}
?>
