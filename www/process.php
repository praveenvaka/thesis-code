<?php
	$upload_dir = "/var/www/upload/";
	$upload_dir_rel = "upload/";
	$work_dir = "/var/www/work/";
	if(!isset($_POST["submit"])){
		if(!isset($_GET["dir"])){
			echo('You should be accessing this page from <a href="index.php">here</a>.');
		}else if(!file_exists($upload_dir.$_GET["dir"])){
			echo('The specified directory doesn\'t exist. <a href="index.php"> Click here</a> to return to the main page.');
		}else{
			echo('<form action="process.php" method="post" enctype="multipart/form-data">
			<label for="size">Maximum size of a region:</label>
			<input type="text" name="size" id="size" value="" />
			<input type="hidden" name="dir" value="'.$_GET["dir"].'" />
			<input type="submit" name="submit" value="Submit" />
			</form><br/>
			The graph representation of the uploaded file is<br/>');
			echo('<img src="'.$upload_dir_rel.$_GET["dir"].'/in.png"/>');			
		}		
		exit();
	}	
	if(!is_numeric($_POST["size"])){
		echo('Size should be a number. Retry again.');
		echo('<form action="process.php" method="post" enctype="multipart/form-data">
		<label for="size">Maximum size of a region:</label>
		<input type="text" name="size" id="size" value="" />
		<input type="hidden" name="dir" value="'.$_POST["dir"].'" />
		<input type="submit" name="submit" value="Submit" />
		</form><br/>
		The graph representation of the uploaded file is<br/>');
		echo('<img src="'.$upload_dir_rel.$_POST["dir"].'/in.png"/>');	
		exit();
	}
	$upload_dir .= $_POST["dir"]."/";
	$upload_dir_rel .= $_POST["dir"]."/";
	exec('python /var/www/main.py '.$upload_dir.'in.dot '.$_POST["size"]);
	rename($work_dir.'output.txt',$upload_dir.'output.txt');
	rename($work_dir.'out.dot',$upload_dir.'out.dot');
	rename($work_dir.'trace.html',$upload_dir.'trace.html');
	exec('dot -Tpng -o '.$upload_dir.'out.png '.$upload_dir.'out.dot');
	exec('python /var/www/default.py '.$upload_dir.'in.dot '.$_POST["size"]);
	//rename($work_dir.'output.txt',$upload_dir.'output_1.txt');
	rename($work_dir.'out.dot',$upload_dir.'out_1.dot');
	rename($work_dir.'trace.html',$upload_dir.'trace_1.html');
	exec('dot -Tpng -o '.$upload_dir.'out_1.png '.$upload_dir.'out_1.dot');
	echo('Try with a different region size.<br/><form action="process.php" method="post" enctype="multipart/form-data">
	<label for="size">Maximum size of a region:</label>
	<input type="text" name="size" id="size" value="" />
	<input type="hidden" name="dir" value="'.$_POST["dir"].'" />
	<input type="submit" name="submit" value="Submit" />
	</form><br/>');
	echo("<pre>");
	//include($upload_dir.'output.txt');
	echo("</pre>");
	echo('<table><tr>');
	echo('<td valign="top">Input Graph<br/><img src="'.$upload_dir_rel.'in.png"/></td>');
	echo('<td valign="top">Output Graph<br/><img src="'.$upload_dir_rel.'out.png"/></td>');
	echo('<td valign="top">Output Graph<br/><img src="'.$upload_dir_rel.'out_1.png"/></td>');
	echo('</tr></table>');
?>
