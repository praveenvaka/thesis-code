#!/usr/bin/env python

import networkx
import sys
import os.path

DEBUG=True
PRINTLVL=0
FILE=""
workfolder="/var/www/work/"
MAX=100
old_nodes=-1
next_node=-1
level2=False
root='1'
ans=0.0

def eval(G,start,prob,path_sum):
	global ans
	path_sum = path_sum + G.node[start]["sum"]+(len(G.successors(start)))*G.node[start]["iter"]
	if len(G.successors(start)) == 0:
		ans = ans + prob*path_sum
		print ans
	else:
		for i in G.successors(start):
			prev = G.predecessors(i)[0]
			edgeprob = float(G.edge[prev][i]['weight'])
			eval(G,i,prob*edgeprob,path_sum)

def main():
	global next_node
	global old_nodes
	global MAX
	global FILE
	global level2
	if DEBUG:
		FILE=open(workfolder+"trace.html","w");
		FILE.write("Debugging Trace<br/>")

	if len(sys.argv) < 2:
		help()
		return

	if not (os.path.isfile(sys.argv[1])):
		print "File \""+ sys.argv[1] + "\" not found"
		help()
		return

	MAX = int(sys.argv[2])

	H=networkx.read_dot(sys.argv[1])
	G=networkx.DiGraph()
	for (u,v,d) in H.edges(data=True):
		G.add_edge(u,v,weight=float(d['weight']),iter=int(d['iter']))
	#networkx.write_dot(G,"graph_in.dot")
	old_nodes = max(map(toint,G.nodes()))
	next_node = old_nodes + 1
	for i in G.nodes():
		G.node[i]["size"]=1
		G.node[i]["ref"]=str(i)
		G.node[i]["iter"]=1
		G.node[i]["sum"]=1
	if debug():
		FILE.write("<b>Start looking for all strongly connected components in the main graph</b><br/>")
	collapse_strong_components(G,G)
	if debug():
		FILE.write("<b>End looking for all strongly connected components in the main graph</b><br/><br/>")
	print networkx.is_directed_acyclic_graph(G)
	readjust_weights(G,'1');
	level2=True
	collapse_all_branches(G,G,'1')
	other(G,G,'1')
	print "Total Regions created: "+str(len(G.nodes()))
	#print "Regions with references to original graph"
	#for i in G.nodes():
	#	print str(i) + ": " + G.node[i]["ref"]
	networkx.write_dot(G,workfolder+"out.dot")
	eval(G,root,1.0,0)
	if debug():
		FILE.close()

def readjust_weights(G,start):
	H=networkx.bfs_edges(G,start)
	for i in networkx.topological_sort(G):
		if len(G.predecessors(i)) > 0:
			to_transfer=0.0
			for (x,y) in G.in_edges(i):
				to_transfer+=G.edge[x][y]['weight']
			for (u,v) in G.out_edges(i):
				G.edge[u][v]['weight']*=to_transfer
	G.node[start]['prob'] = 1
	for i in G.nodes():
		if len(G.predecessors(i))>0:
			prob=0.0
			for j in G.predecessors(i):
				prob += G.edge[j][i]['weight']
			G.node[i]['prob'] = prob


def other(G,H,start):
	sort_order = []
	nodes_to_remove_list = []
	nodes_to_remove=[]
	size=0
	for u in networkx.topological_sort(H,[start]):
		sort_order.append(u)
	for u in sort_order:
		if (size+H.node[u]["size"]) < MAX:
			size += H.node[u]["size"]
			nodes_to_remove.append(u)
		else:
			if len(nodes_to_remove) > 1:
				#collapse(G,H,nodes_to_remove)
				nodes_to_remove_list.append((nodes_to_remove,size))
			nodes_to_remove=[]
			size=0
	if len(nodes_to_remove) > 1:
			nodes_to_remove_list.append((nodes_to_remove,size))

	for (nodes,s) in nodes_to_remove_list:
		collapse(G,nodes,s)
		if G!=H:
			collapse(H,nodes,s)


def collapse(G,nodes,s):
	global next_node
	global root
	if debug():
		FILE.write("Removing nodes "+str(nodes)+"\n")
	new_node=str(next_node)
	next_node+=1
	G.add_node(new_node,size=s,sum=s,ref="")
	p=0.0
	it=1
	for i in nodes:
		if G.node[i]["iter"] > it:
			it = G.node[i]["iter"]
		for j in G.predecessors(i):
			if j not in nodes:
				G.add_edge(j,new_node)
				G.edge[j][new_node]["weight"] = G.edge[j][i]["weight"]
				p += G.edge[j][i]["weight"]
		for j in G.successors(i):
			if j not in nodes:
				G.add_edge(new_node,j,weight=1)
				G.edge[new_node][j]["weight"] = G.edge[i][j]["weight"]
	for i in nodes:
		if i == root:
			root = new_node
		G.node[new_node]["ref"] += ", " if (G.node[new_node]["ref"] != "") else ""
		G.node[new_node]["ref"] += G.node[i]["ref"]
		G.remove_node(i)
	G.node[new_node]["iter"] = it
	if level2:
		G.node[new_node]["prob"] = p

#SCC Functions
def collapse_strong_components(G,H):
	for i in networkx.strongly_connected_component_subgraphs(H):
		if(len(i.nodes()) > 1):
			#if DEBUG:
			#	FILE.write("<br/>")
			if debug():
				FILE.write("SCC: "+str(i.nodes())+"<br/>")
			s=size_component(G,i)
			if s<MAX:
				collapse_component(G,i,s)
			else:
				if debug():
					FILE.write("<i>Size of component is "+str(s)+" and exceeds the maximum size of "+str(MAX)+"</i><br/>")
				for (u,v,d) in i.edges(data=True):
					if not int(d['weight']):
						#networkx.write_adjlist(i,sys.stdout)
						if i.neighbors(u) == [v]:
							start = v
							iter_count = int(G.edge[u][v]["iter"])
							i.remove_edge(u,v)
							G.remove_edge(u,v)
							for j in H:
								G.node[j]["iter"] *= iter_count
								G.node[j]["sum"] *= iter_count
							#networkx.write_adjlist(i,sys.stdout)
							printlvl('+')
							if debug():
								FILE.write("<b>Start looking for strongly connected components in the current component</b><br/>")
							collapse_strong_components(G,i)
							if DEBUG:
								FILE.write("<br/>")
							if DEBUG:
								FILE.write("<br/>")
							if debug():
								FILE.write("<b>End looking for strongly connected components in the current component</b><br/>")
							printlvl('-')
def size_component(G,H):
	size=0
	for i in H:
		size += G.node[i]["size"]
	return size
def sum_component(G,H):
	sum=0
	for i in H:
		sum += G.node[i]["sum"]
	return sum

def collapse_component(G,H,s):
	global next_node
	new_node=str(next_node)
	if debug():
		FILE.write("<i>Collapsed component and replaced by a new node "+new_node+"</i><br/>")
	next_node+=1
	for i in H:
		first_node = i
		break
	G.add_node(new_node,size=s,ref="",sum=sum_component(G,H),iter=G.node[first_node]["iter"])
	for node in H:
		for i in G.successors(node):
			if i not in H:
				G.add_edge(new_node,i)
				G.edge[new_node][i]['weight']=G.edge[node][i]['weight']
				G.edge[new_node][i]['iter']=G.edge[node][i]['iter']
		for i in G.predecessors(node):
			if i not in H:
				G.add_edge(i,new_node)
				#print G.edge[i][node]['weight']
				G.edge[i][new_node]['weight']=G.edge[i][node]['weight']
				G.edge[i][new_node]['iter']=G.edge[i][node]['iter']
	for i in H:
		G.node[new_node]["ref"] += ", " if (G.node[new_node]["ref"] != "") else ""
		G.node[new_node]["ref"] += G.node[i]["ref"]
		G.remove_node(i)


#Branch Related Functions
def collapse_all_branches(G,H,start):
	if debug():
		FILE.write("<b>Start looking for branches</b><br/>")
	L = branches(H,start)
	for(u,v) in L:
		if G.has_node(u) and G.has_node(v):
			collapse_branch(G,u,v)
			if G != H:
				collapse_branch(H,u,v)
	if debug():
		FILE.write("<b>End looking for branches</b><br/>")

def branches(G,start):
	visited = []
	fork = []
	L = []
	u_prev = -1;
	for (u,v) in networkx.dfs_edges(G,start):
		if len(G.successors(u)) == 2:
			if u not in visited:
				visited.append(u)
				fork.append(u)
		else:
			if len(G.predecessors(u)) == 2:
				if(len(fork)> 0):
					L.append((fork.pop(),u))
	return L

def collapse_branch(G,start,end):
	global next_node
	global root
	#if DEBUG:
	#	FILE.write("<br/>")
	if debug():
		FILE.write("Processing branch forking at node "+start+" and joining at node "+end+"<br/>")
	nodes = ret_branch(G,start,end)
	s = size_branch(G,nodes)
	if s < MAX:
		new_node=str(next_node)
		next_node += 1
		if debug():
			FILE.write("<i>Branch structure replaced by a single node "+new_node+"</i><br/>")
		first=G.predecessors(start)[0]
		last=G.successors(end)[0]
		G.add_node(new_node,size=s,sum=s,ref="")
		G.add_edge(first,new_node)
		G.edge[first][new_node]["weight"]=G.edge[first][start]["weight"]
		G.add_edge(new_node,last)
		G.edge[new_node][last]["weight"]=G.edge[end][last]["weight"]
		G.node[new_node]["iter"] = G.node[start]["iter"]
		if level2:
			G.node[new_node]["prob"] = G.node[start]["prob"]
		for i in nodes:
			if i == root:
				root = new_node
			G.node[new_node]["ref"] += ", " if (G.node[new_node]["ref"] != "") else ""
			G.node[new_node]["ref"] += G.node[i]["ref"]
			G.remove_node(i)
	else:
		if debug():
			FILE.write("<i>Size of branch structure is "+str(s)+" and exceeds the maximum size of "+str(MAX)+"</i><br/>")


def ret_branch(G,start,end):
	visited=[]
	toexplore=[]
	visited.append(start)
	toexplore.append(start)
	while(len(toexplore)>0):
		curr=toexplore.pop()
		if curr != end:
			for i in G.successors(curr):
				if i not in visited:
					visited.append(i)
					toexplore.insert(0,i)
	return visited


def size_branch(G,nodes):
	size = 0
	for i in nodes:
		size += G.node[i]["size"]
	return size




#Helper functions
def toint(x):
	return int(x)

def help():
	print "Expected format \"main.py filename maxsize\""
	print "filename is the name of the file containing input graph in dot format"

#Debugging functions
def debug():
	global FILE
	global DEBUG
	global PRINTLVL
	if DEBUG:
		for i in range(PRINTLVL):
			FILE.write("&nbsp;&nbsp;&nbsp;&nbsp;")
		return True
	else:
		return False

def printlvl(do):
	global PRINTLVL
	if do == '+':
		PRINTLVL += 1
	elif do == '-':
		PRINTLVL -=1
	else:
		print "invalid character in arguments to print level"

main()
