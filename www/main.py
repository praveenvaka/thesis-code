#!/usr/bin/env python

import networkx
import sys
import os.path

DEBUG=True
PRINTLVL=0
FILE=""
OUT_FILE=""
workfolder="/var/www/work/"
MAX=100
old_nodes=-1
next_node=-1
ans=0.0
const=3
root='1'
ver=1
D = []

def eval(G,start,prob,path_sum):
	global ans
	path_sum = path_sum + G.node[start]["sum"]+(len(G.successors(start)))*G.node[start]["iter"]
	if len(G.successors(start)) == 0:
		ans = ans + prob*path_sum
		print ans
	else:
		for i in G.successors(start):
			prev = G.predecessors(i)[0]
			edgeprob = float(G.edge[prev][i]['weight'])
			eval(G,i,prob*edgeprob,path_sum)

def main():
	global next_node
	global old_nodes
	global MAX
	global FILE
	global OUT_FILE
	global workfolder

	if DEBUG:
		FILE=open(workfolder+"trace.html","w");
		FILE.write("Debugging Trace<br/>")
	OUT_FILE=open(workfolder+"output.txt","w");
	if len(sys.argv) < 2:
		help()
		return

	if not (os.path.isfile(sys.argv[1])):
		OUT_FILE.write("File \""+ sys.argv[1] + "\" not found")
		help()
		return

	MAX = int(sys.argv[2])
	H=networkx.read_dot(sys.argv[1])
	G=networkx.DiGraph()
	for (u,v,d) in H.edges(data=True):
		G.add_edge(u,v,weight=float(d['weight']),iter=int(d['iter']))
	old_nodes = max(map(toint,G.nodes()))
	next_node = old_nodes + 1
	for i in G.nodes():
		G.node[i]["size"]=1
		G.node[i]["ref"]=str(i)
		G.node[i]["iter"]=1
		G.node[i]["sum"]=1
	if debug():
		FILE.write("<b>Start looking for all strongly connected components in the main graph</b><br/>")
	collapse_strong_components(G,G)
	if debug():
		FILE.write("<b>End looking for all strongly connected components in the main graph</b><br/><br/>")
	#print networkx.is_directed_acyclic_graph(G)
	readjust_weights(G,'1')
	other(G,'1')
	OUT_FILE.write("Total Regions created: "+str(len(G.nodes()))+"\n")
	#OUT_FILE.write("Regions with references to original graph\n")
	#for i in G.nodes():
	#	OUT_FILE.write(str(i) + ": " + G.node[i]["ref"] + "\n")
	networkx.write_dot(G,workfolder+"out.dot")
	eval(G,root,1.0,0)
	if debug():
		FILE.close()
	OUT_FILE.close()

def readjust_weights(G,start):
	H=networkx.bfs_edges(G,start)
	for i in networkx.topological_sort(G):
		if len(G.predecessors(i)) > 0:
			to_transfer=0.0
			for (x,y) in G.in_edges(i):
				to_transfer+=G.edge[x][y]['weight']
			for (u,v) in G.out_edges(i):
				G.edge[u][v]['weight']*=to_transfer
	G.node[start]['prob'] = 1
	for i in G.nodes():
		if len(G.predecessors(i))>0:
			prob=0.0
			for j in G.predecessors(i):
				prob += G.edge[j][i]['weight']
			G.node[i]['prob'] = prob
	#for i in G.nodes():
	#	print i,G.node[i]['prob']

def path_exists(G,n1,n2):
	E = networkx.bfs_predecessors(G, n1)
	flag = False
	for curr, prev in E.iteritems():
		if curr == n2:
			flag = True
			break
	return flag

def create_dict(G):
	global root
	global const
	global D
	del D[:]
	bfs = networkx.bfs_predecessors(G, root)
	#print bfs
	for v in G.nodes():
		for u in G.predecessors(v):
			if len(G.successors(u)) == 1 and len(G.predecessors(v)) == 1:
				if G.node[u]['size']+G.node[v]['size']<MAX:
					p=G.node[u]["prob"]#u
					D.append( (v, ( -1.0*p*const, [u]),p ) )
			else:
				w=G.edge[u][v]["weight"]
				G.remove_edge(u,v)
				if not path_exists(G,u,v):
					if G.node[u]['size']+G.node[v]['size']<MAX:
						p=G.node[u]["prob"]
						s=G.node[u]["size"]
						D.append( (v, ( (1-p)*s-p*const, [u]),p ) )#-p*const
				G.add_edge(u,v)
				G.edge[u][v]["weight"]=w
def get_min():
	global D
	min_val = 10000000
	min_vertex = 0
	min_list = []
	min_prob=0
	for (u,(val,L),p) in D:
		if val < min_val:
			min_val = val
			min_vertex = u
			min_list = L
			min_prob = p
	return (min_val,min_vertex,min_list,min_prob)

def try_collapse(G,u,L,p):
	global next_node
	global root
	global ver
	new_node=str(next_node)
	next_node+=1
	#print "Step: "+str(ver)+" merging "+u+" and ",
	#print L,
	#print " into "+new_node
	if root in L:
		root = new_node
	new_size=G.node[u]['size']
	new_sum=G.node[u]['sum']
	new_iter=1
	for i in L:
		new_size += G.node[i]['size']
		new_sum += G.node[i]['sum']
		if G.node[i]['iter'] > new_iter:
			new_iter = G.node[i]['iter']
	#print "New size is "+str(new_size)
	G.add_node(new_node,size=new_size,ref="",sum=new_sum,iter=1,prob=p)
	#iter don't matter from now on just a dummy attribute
	new_pred = []
	for i in G.predecessors(u):
		if i not in L:
			new_pred.append((i,G.edge[i][u]["weight"]))
	for i in L:
		for j in G.predecessors(i):
			if j not in new_pred:
				new_pred.append((j,G.edge[j][i]["weight"]))
	for (i,w) in new_pred:
		G.add_edge(i,new_node)
		G.edge[i][new_node]["weight"] = w

	new_succ = []
	for i in G.successors(u):
		if i not in L:
			new_succ.append((i,G.edge[u][i]["weight"]))
	for i in L:
		for j in G.successors(i):
			if j not in new_succ:
				new_succ.append((j,G.edge[i][j]["weight"]))
	for (i,w) in new_succ:
		G.add_edge(new_node,i)
		G.edge[new_node][i]["weight"] = w

	G.node[new_node]["ref"] += G.node[u]["ref"]
	G.remove_node(u)
	for i in L:
		G.node[new_node]["ref"] += ", " if (G.node[new_node]["ref"] != "") else ""
		G.node[new_node]["ref"] += G.node[i]["ref"]
		G.remove_node(i)
	ver += 1
	return True

def other(G,start):
	global const
	global root
	global D
	flag = True
	result = False
	root = start
	min_key = -1
	while flag == True:
		result = False
		create_dict(G)
		#print D
		while result == False:
			(m,u,L,p) = get_min()
			#print m,u,L,p
			if m == 10000000:
				flag = False
				break
			else:
				result = try_collapse(G,u,L,p)


#SCC Functions
def collapse_strong_components(G,H):
	for i in networkx.strongly_connected_component_subgraphs(H):
		if(len(i.nodes()) > 1):
			#if DEBUG:
			#	FILE.write("<br/>")
			if debug():
				FILE.write("SCC: "+str(i.nodes())+"<br/>")
			s=size_component(G,i)
			if s<MAX:
				collapse_component(G,i,s)
			else:
				if debug():
					FILE.write("<i>Size of component is "+str(s)+" and exceeds the maximum size of "+str(MAX)+"</i><br/>")
				for (u,v,d) in i.edges(data=True):
					if not int(d['weight']):
						#networkx.write_adjlist(i,sys.stdout)
						if i.neighbors(u) == [v]:
							start = v
							iter_count = int(G.edge[u][v]["iter"])
							i.remove_edge(u,v)
							G.remove_edge(u,v)
							for j in H:
								G.node[j]["iter"] *= iter_count
								G.node[j]["sum"] *= iter_count
							#networkx.write_adjlist(i,sys.stdout)
							printlvl('+')
							if debug():
								FILE.write("<b>Start looking for strongly connected components in the current component</b><br/>")
							collapse_strong_components(G,i)
							if DEBUG:
								FILE.write("<br/>")
							if DEBUG:
								FILE.write("<br/>")
							if debug():
								FILE.write("<b>End looking for strongly connected components in the current component</b><br/>")
							printlvl('-')
def size_component(G,H):
	size=0
	for i in H:
		size += G.node[i]["size"]
	return size
def sum_component(G,H):
	sum=0
	for i in H:
		sum += G.node[i]["sum"]
	return sum

def collapse_component(G,H,s):
	global next_node
	new_node=str(next_node)
	if debug():
		FILE.write("<i>Collapsed component and replaced by a new node "+new_node+"</i><br/>")
	next_node+=1
	for i in H:
		first_node = i
		break
	G.add_node(new_node,size=s,ref="",sum=sum_component(G,H),iter=G.node[first_node]["iter"])
	for node in H:
		for i in G.successors(node):
			if i not in H:
				G.add_edge(new_node,i)
				G.edge[new_node][i]['weight']=G.edge[node][i]['weight']
				G.edge[new_node][i]['iter']=G.edge[node][i]['iter']
		for i in G.predecessors(node):
			if i not in H:
				G.add_edge(i,new_node)
				#print G.edge[i][node]['weight']
				G.edge[i][new_node]['weight']=G.edge[i][node]['weight']
				G.edge[i][new_node]['iter']=G.edge[i][node]['iter']
	for i in H:
		G.node[new_node]["ref"] += ", " if (G.node[new_node]["ref"] != "") else ""
		G.node[new_node]["ref"] += G.node[i]["ref"]
		G.remove_node(i)





#Helper functions
def toint(x):
	return int(x)

def help():
	print "Expected format \"main.py filename maxsize\""
	print "filename is the name of the file containing input graph in dot format"

#Debugging functions
def debug():
	global FILE
	global DEBUG
	global PRINTLVL
	if DEBUG:
		for i in range(PRINTLVL):
			FILE.write("&nbsp;&nbsp;&nbsp;&nbsp;")
		return True
	else:
		return False

def printlvl(do):
	global PRINTLVL
	if do == '+':
		PRINTLVL += 1
	elif do == '-':
		PRINTLVL -=1
	else:
		print "invalid character in arguments to print level"

main()
